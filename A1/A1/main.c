/*
   Interactive 3D Graphics and Animation - Assignment 1: Frogger (part 1)
   Submission done as a pair for:
  - s3607050 - Rei Ito
  - s3429648 - Pacific Thai
*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

#if _WIN32
#   include <Windows.h>
#endif
#if __APPLE__
#   include <OpenGL/gl.h>
#   include <OpenGL/glu.h>
#   include <GLUT/glut.h>
#else
#   include <GL/gl.h>
#   include <GL/glu.h>
#   include <GL/glut.h>
#endif

#ifndef M_PI
#define M_PI 4.0 * atan(1.0)
#endif

typedef struct
{
   float x, y;
} vec2f; //vector struct

typedef struct
{
   vec2f initPos, initVel, currPos, currVel;
} state; //state struct

state projectile;

const float g = -9.8; //gravity
const int milli = 1000; //milliseconds

typedef struct
{
   // boolean flags
   bool debug;
   bool go;
   bool cartesianFlag;
   bool analyticalFlag;
   bool showTangents;
   bool showNormals;
   // values
   float startTime;
   float rotation;
   float speed;
   float size;
   float circleRad;
   int circleSeg;
   int parabolaSeg;
   // restiction variables
   int minSeg;
   int maxSeg;
   float minSpeed;
   float maxSpeed;
   float minRotation;
   float maxRotation;
   // increments
   float speedInc;
   float rotationInc;
} global_t; //global variables

global_t global =
{
   true,     //debug
   false,    //go
   false,    //cartesianFlag
   false,    //analyticalFlag
   true,     //showTangents
   true,     //showNormals
   0.0,      //startTime
   0.785398, //rotation (start at 45)
   1.0,      //speed
   0.1,      //size
   0.05,     //circleRad
   8,        //circleSeg
   8,        //parabolaSeg
   2,        //minSeg
   264,      //maxSeg
   0.5,      //minSpeed
   3.0,      //maxSpeed
   0.0,      //minRotation
   180.0,    //maxRotation
   0.1,      //speedInc
   5.0       //rotationInc
}; //initialised global variables

// initialise initVel
void setInitVel()
{
   projectile.initVel.x = cos(global.rotation) * global.speed;
   projectile.initVel.y = sin(global.rotation) * global.speed;
}

// converts degrees to radians
float degreesToRadians(float degrees) 
{
   float radians = degrees * (M_PI / 180);
   return radians;
}

// takes in required variables and draws a line strip
void drawLine(float startX, float startY, float startZ,
   float endX, float endY, float endZ,
   float r, float g, float b)
{
   glColor3f(r, g, b);
   glBegin(GL_LINE_STRIP);
   glVertex3f(startX, startY, startZ);
   glVertex3f(endX, endY, endZ);
   glEnd();
}

// draws axis lines 
void drawAxis(float length)
{
   drawLine(0, 0, 0, length, 0, 0, 1.0, 0, 0); //x (red)

   drawLine(0, 0, 0, 0, length, 0, 0, 1.0, 0); //y (green)

   drawLine(0, 0, 0, 0, 0, length, 0, 0, 1.0); //z (blue)
}

// draw a vecotr line with given variables
void drawVector(float x, float y, float a, float b, float s,
   bool normalize, float red, float green, float blue)
{
   float magnitude;
   
   if (normalize)
   {
      magnitude = sqrt((a * a) + (b * b));
      a = a / magnitude;
      b = b / magnitude;
   }

   // scales size + attaches to position
   a = (a * (s * global.size)) + x;
   b = (b * (s * global.size)) + y;

   drawLine(x, y, 0, a, b, 0, red, green, blue);
}

// draws a cartesian circle
void drawCartesianCircle()
{
   float x, y;
   float left = -global.circleRad, right = global.circleRad; //start and end point
   float range = right - left; //diameter of circle

   float stepSize = range / global.circleSeg; //segment distance

   // tangent + normal loop
   for (int i = 0; i <= global.circleSeg; i++)
   {
      // coordinates for each point
      x = (i * stepSize) + left;
      y = sqrt((global.circleRad*global.circleRad) - (x*x));

      if (global.showTangents) //tangent check
      {

         drawVector(x + projectile.currPos.x, y + projectile.currPos.y,
            -y, x, 0.5, true, 0.0, 1.0, 1.0); //top half
         drawVector(x + projectile.currPos.x, -y + projectile.currPos.y,
            y, x, 0.5, true, 0.0, 1.0, 1.0); //bottom half
      }
      if (global.showNormals) //normal check
      {
         drawVector(x + projectile.currPos.x, y + projectile.currPos.y,
            x, y, 0.5, true, 1.0, 1.0, 0.0); //top half
         drawVector(x + projectile.currPos.x, -y + projectile.currPos.y,
            x, -y, 0.5, true, 1.0, 1.0, 0.0); //bottom half
      }
   }

   // circle
   glColor3f(1.0, 1.0, 1.0); //set color to white
   glBegin(GL_LINE_STRIP); //top half
   for (int i = 0; i <= global.circleSeg; i++)
   {
      x = (i * stepSize) + left;
      y = sqrt((global.circleRad*global.circleRad) - (x*x));
      glVertex2f(x + projectile.currPos.x, y + projectile.currPos.y);
   }
   glEnd();

   glBegin(GL_LINE_STRIP); //bottom half
   for (int i = 0; i <= global.circleSeg; i++)
   {
      x = (i * stepSize) + left;
      y = sqrt((global.circleRad*global.circleRad) - (x*x));
      glVertex2f(x + projectile.currPos.x, -y + projectile.currPos.y);
   }
   glEnd();
}

// draws a parametric circle
void drawParametricCircle()
{
   float x, y;
   float theta;

   // tangent + normal loop
   for (int i = 0; i <= global.circleSeg * 2; i++)
   {
      theta = i / (float)global.circleSeg * M_PI; //only M_PI to sync seg for each circle
      x = global.circleRad * cosf(theta);
      y = global.circleRad * sinf(theta);

      if (global.showTangents) //tangent check
      {
         drawVector(x + projectile.currPos.x, y + projectile.currPos.y,
            -y, x, 0.5, true, 0.0, 1.0, 1.0);
      }
      if (global.showNormals) //normal check
      {
         drawVector(x + projectile.currPos.x, y + projectile.currPos.y,
            x, y, 0.5, true, 1.0, 1.0, 0.0);
      }
   }

   // circle
   glColor3f(1.0, 1.0, 1.0);
   glBegin(GL_LINE_LOOP);
   for (int i = 0; i <= global.circleSeg * 2; i++)
   {
      theta = i / (float)global.circleSeg * M_PI;
      x = global.circleRad * cosf(theta);
      y = global.circleRad * sinf(theta);
      glVertex2f(x + projectile.currPos.x, y + projectile.currPos.y);
   }
   glEnd();
}

// circle type switcher
void drawCircle(bool cartesianFlag)
{
   if (cartesianFlag)
   {
      drawCartesianCircle();
   }
   else
   {
      drawParametricCircle();
   }
}

// draws a cartesian parabola
void drawCartesianParabola()
{
   float x, y, a, b; // start and end points
   float length = ((sin(2.0 * global.rotation)) * (global.speed * global.speed)) / -g;
   float stepSize = length / (float)global.parabolaSeg;

   setInitVel();

   // tangent + normal
   for (int i = 0; i <= global.parabolaSeg; i++)
   {
      x = i * stepSize;
      y = (tan(global.rotation) * x) + g / (2.0 * global.speed * global.speed *
         cos(global.rotation) * cos(global.rotation)) * (x * x);
      a = 1;
      b = tan(global.rotation) + g / (2.0 * global.speed * global.speed *
         cos(global.rotation) * cos(global.rotation)) * (2 * x);

      if (global.showTangents)
      {
         drawVector(x + projectile.initPos.x, y + projectile.initPos.y,
            a, b, 0.5, true, 0.0, 1.0, 1.0);
      }
      if (global.showNormals)
      {
         drawVector(x + projectile.initPos.x, y + projectile.initPos.y,
            -b, a, 0.5, true, 1.0, 1.0, 0.0);
      }
   }

   // parabola
   glColor3f(0.0, 0.0, 1.0);
   glBegin(GL_LINE_STRIP);
   for (int i = 0; i <= global.parabolaSeg; i++)
   {
      x = i * stepSize;
      y = (tan(global.rotation) * x) + g / (2.0 * global.speed * global.speed * 
         cos(global.rotation) * cos(global.rotation)) * (x * x);

      glVertex3f(x + projectile.initPos.x, y + projectile.initPos.y, 0.0);
   }
   glEnd();
}

// draws a parametric parabola
void drawParametricParabola()
{
   float x, y, a, b; // start + end point

   setInitVel();

   float t, tof = (projectile.initVel.y * 2) / -g; //time of flight

   float stepSize = tof / global.parabolaSeg;

   // tangents + normals
   for (int i = 0; i <= global.parabolaSeg; i++)
   {
      t = i * stepSize;

      x = projectile.initVel.x * t;
      y = (projectile.initVel.y * t) + (0.5 * g * t * t);
      a = projectile.initVel.x;
      b = projectile.initVel.y + g * t;

      if (global.showTangents)
      {
         drawVector(x + projectile.initPos.x, y + projectile.initPos.y,
            a, b, 0.5, true, 0.0, 1.0, 1.0);
      }
      if (global.showNormals)
      {
         // invert normals past 90 degress
         if (global.rotation > degreesToRadians(90))
         {
            drawVector(x + projectile.initPos.x, y + projectile.initPos.y,
               b, -a, 0.5, true, 1.0, 1.0, 0.0);
         }
         else
         {
            drawVector(x + projectile.initPos.x, y + projectile.initPos.y,
               -b, a, 0.5, true, 1.0, 1.0, 0.0);
         }
      }
   }

   // parabola
   glColor3f(0.0, 0.0, 1.0);
   glBegin(GL_LINE_STRIP);
   for (int i = 0; i <= global.parabolaSeg; i++)
   {
      t = i * stepSize;

      x = projectile.initVel.x * t;
      y = (projectile.initVel.y * t) + (0.5 * g * t * t);

      glVertex3f(x + projectile.initPos.x, y + projectile.initPos.y, 0.0);
   }
   glEnd();
}

// parabola type switcher
void drawParabola(bool cartesianFlag)
{
   if (cartesianFlag)
   {
      drawCartesianParabola();
   }
   else
   {
      drawParametricParabola();
   }
}

// analytcal projectile motion
void calcPositionAnalytical(float t)
{
   // position
   projectile.currPos.x = projectile.initVel.x * t + projectile.initPos.x;
   projectile.currPos.y = 
      (projectile.initVel.y * t) + (0.5 * g * t * t) + projectile.initPos.y;
}

// numerical projectile motion
void calcPositionNumerical(float dt) 
{
   // position
   projectile.currPos.x += projectile.currVel.x * dt;
   projectile.currPos.y += projectile.currVel.y * dt;

   // velocity
   projectile.currVel.y += g * dt;
}

// projectile motion type switcher
void calcPosition(bool analyticFlag, float t, float dt)
{
   if (analyticFlag)
   {
      calcPositionAnalytical(t);
   }
   else
   {
      calcPositionNumerical(dt);
   }
}

// update scene
void update(void)
{
   static float lastT = -1.0;
   float t, dt; //time, delta time

   if (!global.go) // check to go
   {
      return;
   }

   t = glutGet(GLUT_ELAPSED_TIME) / (float)milli - global.startTime;

   if (lastT < 0.0) {
      lastT = t;
      return;
   }

   dt = t - lastT;
   if (global.debug) //debuger (code removed since no problems)
   {
   }

   calcPosition(global.analyticalFlag, t, dt); //projectile motion
   lastT = t;

   // hit ground + reset/set variables
   if (projectile.currPos.y < 0.0)
   {
      global.go = false;
      projectile.currPos.y = 0.0;
      projectile.initPos = projectile.currPos;
      projectile.currVel = projectile.initVel;
      lastT = -1.0;
   }

   glutPostRedisplay();
}

// displays everything
void display(void)
{
   GLenum err;

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   glPushMatrix();

   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   drawAxis(1.0); //axis

   drawCircle(global.cartesianFlag); //circles

   drawVector
   (
      projectile.currPos.x, projectile.currPos.y, 
      cos(global.rotation), sin(global.rotation), 
      global.speed, true, 1.0, 0.0, 1.0
   ); //velocity vector

   drawParabola(global.cartesianFlag); //parabolas

   glPopMatrix();

   glutSwapBuffers();

   // check for errors
   while ((err = glGetError()) != GL_NO_ERROR)
      printf("%s\n", gluErrorString(err));
}

// ascii keyboard inputs
void keyboardASCII(unsigned char key, int x, int y)
{
   switch (key) 
   {
   case 'w': //increase speed
      if (global.speed <= global.maxSpeed)
      {
         global.speed += global.speedInc;
      }
      printf("%-12s [%3.2f]\n", "SPEED", global.speed);
      break;
   case 's': //decrease speed
      if (global.speed >= global.minSpeed)
      {
         global.speed -= global.speedInc;
      }
      printf("%-12s [%3.2f]\n", "SPEED", global.speed);
      break;
   case 'a': //increase rotation
      if (global.rotation <= degreesToRadians(global.maxRotation))
      {
         global.rotation += degreesToRadians(global.rotationInc);
      }
      printf("%-12s [%3.2f]\n", "ROTATION", global.rotation);
      break;
   case 'd': //decrease rotation
      if (global.rotation >= degreesToRadians(global.minRotation))
      {
         global.rotation -= degreesToRadians(global.rotationInc);
      }
      printf("%-12s [%3.2f]\n", "ROTATION", global.rotation);
      break;
   case 't': //toggle tangents
      global.showTangents = !global.showTangents;
      printf("%-12s [%s]\n", "showTangents", global.showTangents ? "ON" : "OFF");
      break;
   case 'n': //toggle normals
      global.showNormals = !global.showNormals;
      printf("%-12s [%s]\n", "NORMALS", global.showNormals ? "ON" : "OFF");
      break;
   case 'f': //swap draw mode
      global.cartesianFlag = !global.cartesianFlag;
      printf("%-12s [%s]\n", "DRAW MODE", global.cartesianFlag ? "CARTESIAN" : "PARAMETRIC");
      break;
   case 'i': //swap intergration
      global.analyticalFlag = !global.analyticalFlag;
      printf("%-12s [%s]\n", "INTERGRATION", global.analyticalFlag ? "ANALYTICAL" : "NUMERICAL");
      break;
   case ' ': //jump
      projectile.currVel = projectile.initVel; //set velocity
      if (!global.go) 
      {
         global.startTime = glutGet(GLUT_ELAPSED_TIME) / (float)milli;
         global.go = true;
      }
      break;
   case 'b': //toggle debug
      global.debug = !global.debug;
      printf("%-12s [%s]\n", "DEBUG", global.debug ? "ON" : "OFF");
      break;
   case 'q': //quit
      exit(EXIT_SUCCESS);
      break;
   default:
      break;
   }
   glutPostRedisplay();
}

// special keyboard inputs
void keyboardSpecial(int key, int x, int y)
{
   switch (key)
   {
   case GLUT_KEY_UP: //increase segments
      if (global.circleSeg < global.maxSeg)
      {
         global.circleSeg *= 2;
         global.parabolaSeg *= 2;
      }
      break;
   case GLUT_KEY_DOWN: //decrease segments
      if (global.circleSeg > global.minSeg)
      {
         global.circleSeg /= 2;
         global.parabolaSeg /= 2;
      }
      break;
   default:
      break;
   }
   glutPostRedisplay();
}

// reshapes the scene
void myReshape(int w, int h)
{
   glViewport(0, 0, w, h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();

   glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
}

// main loop
int main(int argc, char** argv)
{
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
   glutInitWindowSize(500, 500);
   glutInitWindowPosition(400, 100);
   glutCreateWindow("Rei & Pacific Assignment 1");
   glutKeyboardFunc(keyboardASCII);
   glutSpecialFunc(keyboardSpecial);
   glutReshapeFunc(myReshape);
   glutDisplayFunc(display);
   glutIdleFunc(update);

   glutMainLoop();
}