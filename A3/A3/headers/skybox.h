#pragma once

#if __cplusplus
extern "C" {
#endif

#include "utils.h"
#include "gl.h"

   typedef struct Skybox Skybox;

   typedef struct Mesh Mesh;
   typedef struct Material Material;
   typedef struct MaterialContainer MaterialContainer;
   typedef struct DebugControls DebugControls;

   struct Skybox
   {
      vec3f pos;

      vec3f size;

      GLuint tex;

      Mesh *mesh;
      Material *material;
   };

   void initSkybox(Skybox *skybox, Mesh *mesh, MaterialContainer *container,
      GLuint tex, vec3f pos, vec3f size);

   void renderSkybox(Skybox *skybox, DebugControls *controls);

#if __cplusplus
}
#endif

