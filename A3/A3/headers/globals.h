#pragma once

#if __cplusplus
extern "C" {
#endif

#include "controls.h"
#include "parabola.h"
#include "player.h"
#include "cars.h"
#include "logs.h"
#include "terrain.h"
#include "skybox.h"
#include "animation.h"
#include "light.h"
#include "camera.h"
#include "material.h"
#include "mesh.h"
#include "utils.h"

#define GLOBAL_MESH 3
#define TERRAIN_AMT 3
#define TEXTURE_AMT 4
#define STR_LEN 20

   typedef struct Globals Globals;


   struct Globals
   {
      Camera camera;

      Terrain terrains[TERRAIN_AMT];

      PlayerControls playerControls;
      CameraControls cameraControls;
      DebugControls debugControls;

      MaterialContainer materials;

      Player player;
      Parabola parabola;

      Car cars;
      Log logs;

      Skybox skybox;

      Animation animation;
      IdleAnimation idleAnimation;

      int frames;
      float frameRate;
      float frameRateInterval;
      float lastFrameRateT;

      Light light;

      Mesh *meshes[GLOBAL_MESH];
      GLuint *textures[TEXTURE_AMT];
   };

   void initGlobals(Globals *globals);
   void freeGlobals(Globals *globals);

#if __cplusplus
}
#endif

