#pragma once

#if __cplusplus
extern "C" {
#endif

#include "utils.h"
#include "animation.h"
#include "../headers/cars.h"
#include "../headers/logs.h"

#define PLAYER_MAT 3
#define MAX_ROT 45.548244
#define MIN_ROT 44
#define MAX_SPD 7
#define MIN_SPD 4.5
#define LIVES_AMT 5
#define WIN_SCORE 5

   typedef enum On On;
   typedef struct Dead Dead;
   typedef struct Player Player;
   typedef struct PlayerControls PlayerControls;
   typedef struct DebugControls DebugControls;
   typedef struct Mesh Mesh;
   typedef struct Material Material;
   typedef struct MaterialContainer MaterialContainer;

   enum On
   {
      NOTHING,
      GRASS,
      ROAD,
      WATER,
      LOG,
   };

   // dead variables
   struct Dead
   {
      vec3f pos; //position of death
      float heading; //direction facing on death
      On on; //on at death
   };

   struct Player
   {
      vec3f pos;
      vec3f vel;
      float size;
      float heading;

      vec3f forward;

      float turnSpeed;

      float jumpRot;
      float jumpSpeed;

      // game statistics
      int lives;
      int score;
      bool end;
      On on;
      bool jumpFlag;

      // count + store dead player positions
      float numDead;
      Dead deadPlayer[LIVES_AMT];

      Mesh *mesh;
      Material *materials[PLAYER_MAT];
   };

   void initPlayer(Player *player, MaterialContainer *container, Mesh *mesh,
      vec3f pos, float size, float turnSpeed, float jumpRot, float jumpSpeed);

   void resetPlayer(Player *player, bool died);
   void renderPlayer(Player *player, Animation *animation,
      IdleAnimation *idleAnimation, DebugControls *controls);

   void renderDead(Player *player, vec3f pos, float rotation,
      DebugControls *controls);
   void renderAllDead(Player *player, DebugControls *controls);

   void updatePlayer(Player *player, Animation *animation,
      IdleAnimation *idleAnimation, PlayerControls *controls, float dt);
   void updatePlayerCollisions(Player *player, Car *cars, Log *logs);

   void setAnimDuration(Player *player, Animation *animation);
   void jump(Player *player, Animation *animation);

   void drawHead(Mesh *mesh, IdleAnimation *idleAnimation,
      Material *materials[], DebugControls *controls);
   void drawLeg(Mesh *mesh, Animation *animation, Material *materials[],
      bool left, DebugControls *controls);
   void drawArm(Mesh *mesh, Animation *animation, Material *materials[],
      bool left, DebugControls *controls);

#if __cplusplus
}
#endif

