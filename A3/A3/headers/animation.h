#pragma once

#if __cplusplus
extern "C" {
#endif

#include "utils.h"
#include "gl.h"

#define NUM_KEYFRAMES 3

   typedef struct Keyframe Keyframe;
   typedef struct Interpolator Interpolator;
   typedef struct Animation Animation;
   typedef struct IdleAnimation IdleAnimation;

   struct Keyframe
   {
       float time;
       float value;
   };

   struct Interpolator
   {
       float rotation; // rotation angle
       float duration, currTime, initialVal; // Interpolation values
       int nKeyFrames; // Current number of keyframes in use
       Keyframe keyFrames[NUM_KEYFRAMES]; // Array of keyframes (with cap)
   };
   
   struct Animation
   {
       Interpolator body;
       Interpolator shoulder;
       Interpolator elbow;
       Interpolator upperLeg;
       Interpolator middleLeg;
       Interpolator lowerLeg;
   };

   struct IdleAnimation
   {
       Interpolator wheels;
       Interpolator mouth;
       float ribbitInterval;
       bool isRibbiting;
   };

   void initAnimation(Animation *animation);
   void initIdleAnimation(IdleAnimation *idleAnimation);

   float lerp(float t0, float v0, float t1, float v1, float t);
   void interpolate(Interpolator *in, float dt);

   void jumpAnimation(Animation *animation, float dt);
   void ribbitAnimation(IdleAnimation *idleAnimation, float dt);

#if __cplusplus
}
#endif