##### Interactive 3D Graphics and Animation Assignment 3: Frogger (part 3) #####
Submission done as a pair for:
  - s3607050 - Rei Ito
  - s3429648 - Pacific Thai

NOTE: Provided assignment 2 code was used as a base for this assignment
all comments contained were removed to differectiate from our own
  
[1. Completed requirements]
    [A. Road and River]
        - Added road and river, both with textures
    
    [B. Collision Detection]
        - Performed collisions between cars and frog, if frog collides with car
    
    [C. Hierarchical model]
        - A hierarchical model has been completed for the frog
        - A hierarchical model has been completed for the cars
    
    [D. Keyframe animation]
        - Added to frog when jumping, reset every time frog is jumping
        - Added to frog at random interval ribbiting
     
    [E. OSD]
        - OSD displays lives
        - OSD displays score
        - OSD displays frame rate
        - OSD displays time per frame
        - OSD displays game name (Frogger)
    
    [F. 2D/3D effects]
        - A 2D effect has been added when frog collides with car, a splattered frog is generated at the spot of collision
     
    [G. Gameplay]
        - Maximum jump distance has been restricted, frog cannot jump over river or road in a single bound
        - Score is updated when frog reaches end of area
        - A life is subtracted if the frog collides with a car and if it jumps into the river
        - Game ends when all lives are lost
        
    
[2. Additions]
    - Wheels on cars have been animated
  
[3. Extra Notes]
    - If frog jumps outside of level area, it counts as a death and a life is lost
	- There's no way to get points to the game due to the logs not working, to test that it does work, in player.c: line 307,
      comment out 'resetPlayer' and jump to the end of the level. 
    
[4. Incomplete]
    - Collision between frog and logs, frog is unable to stick onto logs and move with them
	- Particle effects when player dies
	- An indented river bed below the grass
	- Skybox