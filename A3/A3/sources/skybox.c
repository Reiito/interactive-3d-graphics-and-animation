#include "../headers/skybox.h"
#include "../headers/mesh.h"
#include "../headers/material.h"
#include "../headers/gl.h"

void initSkybox(Skybox *skybox, Mesh *mesh, MaterialContainer *container,
   GLuint tex, vec3f pos, vec3f size)
{
   skybox->pos = pos;
   skybox->size = size;

   skybox->tex = tex;

   skybox->mesh = mesh;
   skybox->material = getMaterial(container, "blue");
}

void renderSkybox(Skybox *skybox, DebugControls *controls)
{
   glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_COLOR_BUFFER_BIT);

   glBindTexture(GL_TEXTURE_2D, skybox->tex);

   glPushMatrix();

   glTranslatef(skybox->pos.x, skybox->pos.y, skybox->pos.z);
   renderMesh(skybox->mesh, skybox->material, skybox->size, controls);

   glPopMatrix();

   glPopAttrib();
}

