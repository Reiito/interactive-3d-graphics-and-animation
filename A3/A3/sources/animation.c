#include "../headers/animation.h"

void initAnimation(Animation *animation)
{
    Interpolator in =
    {
        0, //rotation
        2, 0, 0, //duration, currTime, initialVal
        3, //nKeyFrames
        {
            { 0.0, 0.0 }, //starting keyFrame
            { 0.5, 0.0 }, //middle keyFrame
            { 1.0, 0.0 }, //ending keyFrame (move back to initial spot)
        }
    };

    // set base start, mid and end keyframes for each limb
    animation->body = in;
    animation->shoulder = in;
    animation->elbow = in;
    animation->upperLeg = in;
    animation->middleLeg = in;
    animation->lowerLeg = in;

    // set unique middle second frame rotations
    // [a] torso
    animation->body.keyFrames[1].value = -45.0; //body
    // [b] arms
    animation->shoulder.keyFrames[1].value = 30.0; //shoulder/upper arm
    animation->elbow.keyFrames[1].value = 90.0; //elbow/middle arm
    // [c] legs
    animation->upperLeg.keyFrames[1].value = 100.0; //hip/upper leg
    animation->middleLeg.keyFrames[1].value = -90.0; //knee/middle leg
    animation->lowerLeg.keyFrames[1].value = 60.0; //ankle/lower leg
}

void initIdleAnimation(IdleAnimation *idleAnimation)
{
    float milli = 1000;
    Interpolator in =
    {
        0, //rotation
        2, 0, 0, //duration, currTime, initialVal
        3, //nKeyFrames
        {
            { 0.0, 0.0 }, //starting keyFrame
            { 0.5, 0.0 }, //middle keyFrame
            { 1.0, 0.0 }, //ending keyFrame (move back to initial spot)
        }
    };
    idleAnimation->mouth = in;
    idleAnimation->wheels = in;

    // set mouth values
    idleAnimation->mouth.duration = 0.5;
    idleAnimation->mouth.keyFrames[1].value = 15.0;

    // set values for wheel
    idleAnimation->wheels.keyFrames[1].value = -180.0;
    idleAnimation->wheels.keyFrames[2].value = -360.0;

    idleAnimation->isRibbiting = false;
    idleAnimation->ribbitInterval = getTRand(3, 10) * milli;
}

float lerp(float t0, float v0, float t1, float v1, float t)
{
    return (v1 - v0) / (t1 - t0) * (t - t0) + v0;
}

void interpolate(Interpolator *in, float dt) 
{
    // reset animation variables once time reaches duration
    if (in->currTime >= in->duration)
    {
        in->currTime = 0;
        return;
    }

    Keyframe curr, prev;
    float rotation;

    // find current keyframe that interpolator is to animate
    for (int i = 0; i < in->nKeyFrames; i++)
    {
        if ((in->keyFrames[i].time * in->duration) >= in->currTime)
        {
            prev = in->keyFrames[i - 1];
            curr = in->keyFrames[i];
            break;
        }
    }

    // animate frog limb based on total duration of animation
    if (in->currTime <= in->duration)
    {
        rotation = lerp(prev.time * in->duration, prev.value,
                        curr.time * in->duration, curr.value, in->currTime);
        in->currTime += dt;

        if (!isnan(rotation)) //skip nan outputs (makes part flash)
            in->rotation = rotation;
    }
}

void jumpAnimation(Animation *animation, float dt)
{
    interpolate(&animation->body, dt);
    interpolate(&animation->shoulder, dt);
    interpolate(&animation->elbow, dt);
    interpolate(&animation->upperLeg, dt);
    interpolate(&animation->middleLeg, dt);
    interpolate(&animation->lowerLeg, dt);
}

void ribbitAnimation(IdleAnimation *idleAnimation, float dt)
{
    if (idleAnimation->isRibbiting)
    {
        interpolate(&idleAnimation->mouth, dt);
        // Reset animation at end
        if (idleAnimation->mouth.currTime >= idleAnimation->mouth.duration)
        {
            idleAnimation->mouth.rotation = 0;
            idleAnimation->mouth.currTime = 0;
            idleAnimation->isRibbiting = false;
        }
    }
}
