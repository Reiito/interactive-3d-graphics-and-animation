#include "../headers/globals.h"
#include "../headers/gl.h"

Globals globals;

static float getDeltaTime()
{
   static int t1 = -1;

   if (t1 == -1)
      t1 = glutGet(GLUT_ELAPSED_TIME);
   int t2 = glutGet(GLUT_ELAPSED_TIME);
   float dt = (t2 - t1) / 1000.0f;
   t1 = t2;

   return dt;
}

// ribbit animation call
static void randomRibbit()
{
   float dt = getDeltaTime();
   float milli = 1000.0;
   globals.idleAnimation.isRibbiting = true;
   globals.idleAnimation.ribbitInterval = getTRand(3, 10) * milli;
   glutTimerFunc(globals.idleAnimation.ribbitInterval, randomRibbit, 0);
}

// frame reate updater
static void updateFrameRate()
{
   static float lastT = -1.0;
   float t, dt;

   t = glutGet(GLUT_ELAPSED_TIME) / (float)1000 - 0;

   if (lastT < 0.0)
   {
      lastT = t;
      return;
   }

   dt = t - lastT;
   lastT = t;

   // frame rate
   dt = t - globals.lastFrameRateT;
   if (dt > globals.frameRateInterval)
   {
      globals.frameRate = globals.frames / dt;
      globals.lastFrameRateT = t;
      globals.frames = 0;
   }
}

static void updateKeyInt(int key, bool state)
{
   switch (key)
   {
   case GLUT_KEY_UP:
      globals.playerControls.up = state;
      break;

   case GLUT_KEY_DOWN:
      globals.playerControls.down = state;
      break;

   case GLUT_KEY_LEFT:
      globals.playerControls.left = state;
      break;

   case GLUT_KEY_RIGHT:
      globals.playerControls.right = state;
      break;

   default:
      break;
   }
}

static void updateKeyChar(unsigned char key, bool state)
{
   switch (key)
   {
   case 'w':
      globals.playerControls.upSpeed = state;
      break;

   case 's':
      globals.playerControls.downSpeed = state;
      break;

   case 'a':
      globals.playerControls.upRot = state;
      break;

   case 'd':
      globals.playerControls.downRot = state;
      break;

   default:
      break;
   }
}

// show gameplayer stats + frame rate
void displayOSD()
{
   char buffer[30];
   char *bufp;
   int w, h, wPos; //window width, height and width position to draw text
   void* font = GLUT_BITMAP_HELVETICA_10; //font used
   vec3f textColor = { 0.0, 1.0, 0.0 }; //green color for text

   glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
   glDisable(GL_DEPTH_TEST);
   glDisable(GL_LIGHTING);

   glMatrixMode(GL_PROJECTION);

   // [1] draw OSD
   glPushMatrix();
      glLoadIdentity();

      // Get window coordinates to draw text
      w = glutGet(GLUT_WINDOW_WIDTH);
      h = glutGet(GLUT_WINDOW_HEIGHT);
      glOrtho(0.0, w, 0.0, h, -1.0, 1.0);

      glMatrixMode(GL_MODELVIEW);

      // [2] draw display
      glPushMatrix();
         glLoadIdentity();

          wPos = (w / 2.0) - 30; //set width position based on window size

         // [a] game name
         glColor3fv((float*)&textColor);
         glRasterPos2i(wPos + 8, h - 20); //center "Frogger" text
         snprintf(buffer, sizeof buffer, "Frogger");
         for (bufp = buffer; *bufp; bufp++)
            glutBitmapCharacter(font, *bufp);

         // [b] lives left
         glColor3fv((float*)&textColor);
         glRasterPos2i(wPos, h - 40);
         snprintf(buffer, sizeof buffer, "Lives: %d", globals.player.lives);
         for (bufp = buffer; *bufp; bufp++)
            glutBitmapCharacter(font, *bufp);

         // [c] scoring
         glColor3fv((float*)&textColor);
         glRasterPos2i(wPos, h - 60);
         snprintf(buffer, sizeof buffer, "Score: %d", globals.player.score);
         for (bufp = buffer; *bufp; bufp++)
            glutBitmapCharacter(font, *bufp);

         // [d] frame rate
         glColor3fv((float*)&textColor);
         glRasterPos2i(wPos, h - 80);
         snprintf(buffer, sizeof buffer, "fr (f/s): %6.0f", globals.frameRate);
         for (bufp = buffer; *bufp; bufp++)
            glutBitmapCharacter(font, *bufp);

         // [e] time per frame
         glColor3fv((float*)&textColor);
         glRasterPos2i(wPos, h - 100);
         snprintf(buffer, sizeof buffer, "ft (ms/f): %5.0f",
            1.0 / globals.frameRate * 1000.0);
         for (bufp = buffer; *bufp; bufp++)
            glutBitmapCharacter(font, *bufp);

         // [f] game end message
         if (globals.player.end)
         {
            glColor3fv((float*)&textColor);
            glRasterPos2i(wPos, h - 120);
            if (globals.player.score == WIN_SCORE)
            {
               snprintf(buffer, sizeof buffer, "YOU WIN!");
            }
            else if (globals.player.lives == 0)
            {
               snprintf(buffer, sizeof buffer, "YOU LOSE!");
            }
            for (bufp = buffer; *bufp; bufp++)
            {
               glutBitmapCharacter(font, *bufp);
            }
         }
      glPopMatrix(); //[2]
   glMatrixMode(GL_PROJECTION);
   glPopMatrix(); //[1]
   glMatrixMode(GL_MODELVIEW);
   glPopAttrib();
}

static void render(void)
{
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   uploadProjection(&globals.camera);
   uploadModelview(&globals.camera);

   // light
   renderLight(&globals.light);

   // planes
   renderTerrain(&globals.terrains[0], &globals.debugControls);
   renderTerrain(&globals.terrains[1], &globals.debugControls);
   renderTerrain(&globals.terrains[2], &globals.debugControls);

   // player
   if (!globals.player.end)
   {
      renderPlayer(&globals.player, &globals.animation, &globals.idleAnimation,
         &globals.debugControls);
      renderParabola(&globals.parabola, &globals.debugControls);
   }
   renderAllDead(&globals.player, &globals.debugControls);

   // obstacles
   renderCars(&globals.cars, &globals.idleAnimation, &globals.debugControls);
   renderLogs(&globals.logs, &globals.debugControls);

   // skybox
   //renderSkybox(&globals.skybox, &globals.debugControls);

   // OSD
   displayOSD();

   glutSwapBuffers();

   globals.frames++; //increment frames
}

static void update(void)
{
   float dt = getDeltaTime();

   // end condition
   if (globals.player.end)
      return;

   // framerate
   updateFrameRate();

   // player
   updatePlayer(&globals.player, &globals.animation, &globals.idleAnimation,
      &globals.playerControls, dt);

   // obsticals
   updateCars(&globals.cars, &globals.idleAnimation, dt);
   updateLogs(&globals.logs, dt);

   // parabola set
   if (globals.player.pos.y <= 0)
      globals.parabola.pos = globals.player.pos;

   updatePlayerCollisions(&globals.player, &globals.cars, &globals.logs);

   glutPostRedisplay();
}

static void reshape(int width, int height)
{
   globals.camera.right = width;
   globals.camera.height = height;

   render();
}

static void mouseMove(int x, int y)
{
   static int prevX, prevY;

   int dx = x - prevX;
   int dy = y - prevY;

   if (globals.cameraControls.rotating)
      updateRotation(&globals.camera, dy, dx);

   if (globals.cameraControls.zooming)
      updateZoom(&globals.camera, dy);

   prevX = x;
   prevY = y;
}

static void mouseDown(int button, int state, int x, int y)
{
   UNUSED(x);
   UNUSED(y);

   if (button == GLUT_LEFT_BUTTON)
      globals.cameraControls.rotating = state == GLUT_DOWN;
   if (button == GLUT_RIGHT_BUTTON)
      globals.cameraControls.zooming = state == GLUT_DOWN;
}

static void keyboardDown(unsigned char key, int x, int y)
{
   UNUSED(x);
   UNUSED(y);

   switch (key)
   {
   case 27:
   case 'q':
      exit(EXIT_SUCCESS);
      break;

   case 'p':
      globals.debugControls.wireframeFlag =
         !globals.debugControls.wireframeFlag;
      toggleWireframe(&globals.debugControls);
      break;

   case 'l':
      globals.debugControls.lightingFlag = !globals.debugControls.lightingFlag;
      toggleLighting(&globals.debugControls);
      break;

   case 'n':
      globals.debugControls.normalFlag = !globals.debugControls.normalFlag;
      break;

   case 't':
      globals.debugControls.textureFlag = !globals.debugControls.textureFlag;
      toggleTexturing(&globals.debugControls);
      break;

   case 'o':
      globals.debugControls.axisFlag = !globals.debugControls.axisFlag;
      break;

   case ' ':
      jump(&globals.player, &globals.animation);
      break;

   default:
      updateKeyChar(key, true);
      break;
   }
}

static void keyboardUp(unsigned char key, int x, int y)
{
   UNUSED(x);
   UNUSED(y);

   updateKeyChar(key, false);
}

static void keyboardSpecialDown(int key, int x, int y)
{
   UNUSED(x);
   UNUSED(y);

   updateKeyInt(key, true);
}

static void keyboardSpecialUp(int key, int x, int y)
{
   UNUSED(x);
   UNUSED(y);

   updateKeyInt(key, false);
}

static void cleanup(void)
{
   freeGlobals(&globals);
}

int main(int argc, char **argv)
{
   initCamera(&globals.camera, 0, 640, 480);

   glutInit(&argc, argv);
   glutInitWindowSize(globals.camera.right, globals.camera.height);
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
   glutCreateWindow("I3D Assignment 3");

   glutDisplayFunc(render);
   glutIdleFunc(update);
   glutTimerFunc(globals.idleAnimation.ribbitInterval, randomRibbit, 0);
   glutReshapeFunc(reshape);
   glutMotionFunc(mouseMove);
   glutPassiveMotionFunc(mouseMove);
   glutMouseFunc(mouseDown);
   glutKeyboardFunc(keyboardDown);
   glutKeyboardUpFunc(keyboardUp);
   glutSpecialFunc(keyboardSpecialDown);
   glutSpecialUpFunc(keyboardSpecialUp);

   atexit(cleanup);

   initGlobals(&globals);

   glutMainLoop();

   return EXIT_SUCCESS;
}

